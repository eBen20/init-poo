import java.util.ArrayList;

class LibEntiers{
    public static int somme(int x, int y){
        return x + y;
    }

    public static int somme(ArrayList<Integer> liste){
        int total = 0;
        if (liste.size() == 0){
            return total;
        }
        for (int nombre : liste){
            total += nombre;
        }
        return total;
    }

    public static int exponentiation(int x, int y){
        return (int) Math.pow(x, y);
    }

    public static int min(ArrayList<Integer> liste){
        Integer min =  null;
        if (liste.size() == 0){
            return 0;
        }
        for (int nombre : liste){
            if (min == null || min > nombre){
                min = nombre;
            }
        }
        return min;
    }
}