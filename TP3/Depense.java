import src.Personne;

class Depense {
    private double montant;
    private String produit;
    private Personne qui;
    
    public Depense(String produit, int montant, Personne qui){
        this.montant = montant;
        this.produit = produit;
        this.qui = qui;
    }

    public Personne getQui(){
        return this.qui;
    }

    public double getMontant(){
        return this.montant;
    }

    public String getProduit(){
        return this.produit;
    }

    @Override
    public String toString(){
        return this.qui + " à payer" + this.montant + "€ pour " + this.produit;
    }
}

