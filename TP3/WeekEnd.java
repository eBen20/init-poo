import java.util.ArrayList;
import java.util.List;

import src.Personne;

public class WeekEnd {
    private List<Personne> listeAmis;
    private List<Depense> listeDepenses;

    public WeekEnd(){
        this.listeAmis = new ArrayList<>();
        this.listeDepenses= new ArrayList<>();
    }

    

    public void ajouterPersonne(Personne personne){
        this.listeAmis.add(personne);
    }

    

    public void ajouterDepense(Depense dpense){
        this.listeDepenses.add(dpense);
    }

    

    // totalDepensesPersonne prend paramètre une personne // et renvoie la somme des dépenses de cette personne.

    public int depenseParPersonne(Personne personne){
        int depPers = 0;
        for (Depense depense : this.listeDepenses){
            if (depense.getQui().equals(personne)){
                depPers += depense.getMontant();
            }
        }
        return depPers;
    }

    

    // totalDepenses renvoie la somme de toutes les dépenses.

    public double totalDepenses(){
        double totalDep = 0;
        for (Depense depense : this.listeDepenses){
            totalDep += depense.getMontant();
        }
        return totalDep;
    }

    

    // depenseMoyenne renvoie la moyenne des dépenses par // personne

    public double depenseMoyenne(){
        double moyenne = 0;
        for (Depense depense : this.listeDepenses){
            moyenne += depense.getMontant();
        }
        moyenne = moyenne / this.listeAmis.size();
        return moyenne;
    }

    

    // totalDepenseProduit prend un nom de produit en paramètre et renvoie la // somme des dépenses pour ce produit. // (du pain peut avoir été acheté plusieurs fois...)

    public double totalDepenseProduit(String nomProduit){
        double total = 0;
        for (Depense depense : this.listeDepenses){
            if (depense.getProduit().equals(nomProduit)){
                total += depense.getMontant();
            }
        }
        return total;
    }
   

    // avoirParPersonne prend en paramètre une personne et renvoie // son avoir pour le week end.

    public double avoirPersonne(Personne personne){
        return this.depenseMoyenne() - this.depenseParPersonne(personne);
    } 

    @Override
    public String toString(){
        return "Le weekend est composé de " + this.listeAmis + ", il ont dépensés:" + this.listeDepenses;
    }
}

