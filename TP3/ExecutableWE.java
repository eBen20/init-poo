import src.Personne;

class ExecutableWE {
    public static void main(String [] arg){
        Personne pierre = new Personne("Pierre");
        Depense pain = new Depense("Pain", 12, pierre);
        Personne paul = new Personne("Paul");
        Depense pizza = new Depense("Pizzas", 100, paul);
        Depense essence = new Depense("Essence", 70, pierre);
        Personne marie = new Personne("Marie");
        Depense vin = new Depense("Vin", 15, marie);
        Depense vin2 = new Depense("Vin", 10, paul);
        Personne anna = new Personne("Anna");

        System.out.println(pain.getQui());

        WeekEnd week = new WeekEnd();
        week.ajouterPersonne(pierre);
        week.ajouterPersonne(paul);
        week.ajouterPersonne(marie);
        week.ajouterPersonne(anna);
        week.ajouterDepense(pain);
        week.ajouterDepense(pizza);
        week.ajouterDepense(essence);
        week.ajouterDepense(vin);
        week.ajouterDepense(vin2);

        System.out.println(week.totalDepenses());
        System.out.println(week.depenseParPersonne(paul));
        System.out.println(week.depenseMoyenne());
        System.out.println(week.totalDepenseProduit("Vin"));
        System.out.println(week.avoirPersonne(anna));
        
    }

    
}
