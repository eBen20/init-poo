package src;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.*;

public class Personne implements Comparable<Personne> {
    private String nom;
    private int age;

    public Personne(String nom, int age){
        this.nom = nom;
        this.age = age;
    }

    public Personne(String string) {
    }

    public int getAge(){
        return this.age;
    }

    //Permet de trié une liste de Personne par ordre croissant de leur age
    public static List<Personne> triOrdreCroissantAge(List<Personne> listePers){
        Collections.sort( listePers);
        return listePers;
    }
    
    // public int ecart(List<Personne> listPers){
    //     Integer min = null;
    //     for (Personne pers : listPers){
    //         for (Personne pers2 : listPers){

    //         }
    //     }
    // }

    public int compareTo(Personne pers){
        if (pers.age > this.age){
            return -1;
        }
        else if (pers.age == this.age){
            return 0;
        }
        else {
            return 1;
        }
    }

    @Override
    public boolean equals(Object obj){
        if (obj == null){
            return false;
        }
        if (obj == this){
            return true;
        }
        if (obj instanceof Personne){
            Personne pers = (Personne) obj;
            if (pers.age == this.age && pers.nom.equals(this.nom)){
                return true;
            }
        }
        return false;
    }

}
