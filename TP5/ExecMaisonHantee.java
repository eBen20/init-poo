public class ExecMaisonHantee{
    public static void main(String [] args) {
        Fantome revenant = new Fantome("Le Revenant", 3);
        Fantome spectre = new Fantome("Le Revenant", 3); // le même qu'au dessus
        Fantome macheur = new Fantome("Le Mâcheur", 1);
        Fantome frappeur = new Fantome("Le Frappeur", 5);
        Fantome vortex = new Fantome("Le Vortex", 4);
        MaisonHantee haloween = new MaisonHantee("Haloween");
        haloween.ajouter(revenant);
        haloween.ajouter(spectre);
        haloween.ajouter(macheur);
        haloween.ajouter(frappeur);
        haloween.ajouter(vortex);
        System.out.println(haloween);
        /*
        Haloween hantée par [Fantome Le Revenant de force 3,
                             Fantome Le Mâcheur de force 1,
                             Fantome Le Frappeur de force 5,
                             Fantome Le Vortex de force 4]*/
  
        haloween.bataille(revenant, vortex);
        System.out.println(haloween);
  
        /*
        Haloween hantée par [Fantome Le Revenant de force 3,
                             Fantome Le Mâcheur de force 1,
                             Fantome Le Frappeur de force 5,
                             Fantome Le Vortex de force 5]
        */
    }
}