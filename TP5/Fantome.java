
public class Fantome {
    private String nom;
    private int force;

    public Fantome(String nom, int force){
        this.nom = nom;
        this.force = force;
    }

    public int getForce() {
        return this.force;
    }

    public String getNom() {
        return this.nom;
    }

    public void setForce(int force) {
        this.force = force;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public String toString(){
        return "Le fantome " + this.nom + " avec une force de " + this.force + "\n";
    }

    @Override
    public boolean equals(Object fantome){
        if (fantome == null){
            return false;
        }
        if (!(fantome instanceof Fantome)){
            return false;
        }
        if (this.nom.equals(((Fantome) fantome).getNom()) && this.force == ((Fantome) fantome).getForce()){
            return true;
        }
        return false;
    }
}
