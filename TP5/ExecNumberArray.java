import java.util.ArrayList;
import java.util.List;

public class ExecNumberArray
{
  public static void main(String [] args)
  {
    List<Number> tableau = new ArrayList<Number>();
    tableau.add(5);
    tableau.add(6.);
    tableau.add(7.f);
    Number number = 8.5f;
    tableau.add(number);
    int res = 0;
    for (Number nb : tableau){
        res += nb.intValue();
    }
    System.out.println(res);
    System.out.println(tableau); /*On fait appel au toString de l'ArrayList, et des différents type de nombre*/
    
    /*Le code suivant va écrire False car ce n'est pas le même type*/
    Number x = new Integer(5);
    Number y = new Double(5);
    System.out.println(x.equals(y));
  }
}