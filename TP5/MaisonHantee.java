import java.util.ArrayList;
import java.util.List;

public class MaisonHantee {
    private String nom;
    private List<Fantome> listeFantom;

    public MaisonHantee(String nom){
        this.nom = nom;
        this.listeFantom = new ArrayList<Fantome>();
    }
    
    public List<Fantome> getListeFantom() {
        return this.listeFantom;
    }

    public String getNom() {
        return this.nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void bataille(Fantome fantome, Fantome fantome2){
        if (fantome.getForce() > fantome2.getForce()){
            fantome.setForce(fantome.getForce() + 1);
        }
        else if (fantome.getForce() < fantome2.getForce()){
            fantome2.setForce(fantome2.getForce() + 1);
        }
    }

    public void ajouter(Fantome fantome){
        if (!listeFantom.contains(fantome)){
            this.listeFantom.add(fantome);
        }
    }

    @Override
    public String toString(){
        return "La maison contient les fantomes: " + listeFantom.toString();
    }
}
