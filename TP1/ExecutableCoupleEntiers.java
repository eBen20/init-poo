public class ExecutableCoupleEntiers {
    public static void main(String [] args) {
        CoupleEntiers couple = new CoupleEntiers();
        couple.setPrem(-16);
        System.out.println(couple.toString()); // (1)
        System.out.println(couple.fraction()); // (2)

        couple.setSec(-6);
        System.out.println(couple.getPrem()+" "+couple.getSec());
        System.out.println(couple.somme()); // affiche -22

        //def du second CoupleEntier
        CoupleEntiers couple2 = new CoupleEntiers();
        couple2.setPrem(1);
        couple2.setSec(3);
        CoupleEntiers couple3 = couple.plus(couple, couple2);
        System.out.println(couple3.toString()); // (-15,-3)
        
    }
 }
 