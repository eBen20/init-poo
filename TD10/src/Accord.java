package src;
import java.util.HashSet;

public class Accord extends HashSet<Note> implements Jouable{

    public void jouer(){
        System.out.println("---------début-----------");
        for(Note note : this){
            note.jouer();
        }
        System.out.println("---------fin-----------");
    }

    public int duree(){
        int dureeMax = 0;
        for(Note note : this){
            int dureeNote = note.getDuree();
            if(dureeNote > dureeMax){
                dureeMax = dureeNote; 
            }
        }
        return dureeMax;
    }

    public boolean estHarmonieux(){
        for(Note note : this){
            if(!Accord.estPuisDe2(note.getFrequence())){
                return false;
            }
        }
        return false;
    }

    public static boolean estPuisDe2(int nb){
        int x = 1;
        while (x <= nb){
            if(x == nb){
                return true;
            }
            x = x * 2;
        }
        return false;
    }

    public static void main(String[] args) {
        Accord accord0 = new Accord();
        accord0.add(new Note(1, 2));
        accord0.add(new Note(2, 5));
        accord0.add(new Note(9, 4));

        accord0.jouer();
        System.out.println(!accord0.estHarmonieux());
        System.out.println(accord0.duree() == 5);
        System.out.println(!Accord.estPuisDe2(68));

    }
}
