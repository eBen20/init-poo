import java.util.ArrayList;
import java.util.List;

public class Enclos {
    private String nom;
    private float superficie;
    private List<Animal> listAnimaux;

    public  Enclos(String nom, float superficie){
        this.nom = nom;
        this.superficie = superficie;
        this.listAnimaux = new ArrayList<>();
    }
    public String getNom(){
        return this.nom;
    }
    public float getSuperficie() {
        return this.superficie;
    }

    public List<Animal> getListAnimaux() {
        return this.listAnimaux;
    }
    public void ajouterAnimal(Animal animal){
        this.listAnimaux.add(animal);
    }
    @Override
    public String toString(){
        return "Dans l'enclos " + this.nom + " de " + this.superficie + "m2 il y " + this.listAnimaux;
    }
}
