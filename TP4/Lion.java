public class Lion extends Animal{
    private Boolean aCriniere;



    public Lion(String nom, float poid, Zoo zoo, Boolean acrini){
        super(nom, poid, zoo);
        this.aCriniere = acrini;
    }

    public Boolean getCrini(){
        return this.aCriniere;
    }

    @Override
    public void setEnclos(Enclos newEnclos) {
        super.setEnclos(newEnclos);
    }

    @Override
    public String toString(){
        String res = super.toString();
        if (this.aCriniere) {
            return "Lion " + res + " à une crinière";
        }
        else {
             return "Lion " + res + " n'à pa de crinière";
        }
    }

    @Override
    public String emmitionCrie() {
        return "Graou";
    }
}
