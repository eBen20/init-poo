public class Serpent extends Animal {
    private Boolean venimeu;

    public Serpent(String nom, float poid, Boolean venim, Zoo zoo){
       super(nom, poid, zoo);
       this.venimeu = venim;
    }
    public Boolean getEstVenimeu(){
        return this.venimeu;
    }
    @Override
    public void setEnclos(Enclos newEnclos) {
        super.setEnclos(newEnclos);
    }
    @Override
    public String toString(){
        String res = super.toString();
        if (this.venimeu) {
            return "Serpent " + res + " est venimeu";
        }
        else {
            return "Serpent " + res + " n'est pas venimeu";
        }
    }
    @Override
    public String emmitionCrie() {
        return "tssssss";
    }
}
