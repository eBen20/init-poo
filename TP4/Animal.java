public abstract class Animal {
    private String nom;
    private float poid;
    private Enclos enclos;
    private Zoo zoo;

    public  Animal(String nom, float poid, Enclos enclo, Zoo zoo){
        this.nom = nom;
        this.poid = poid;
        this.zoo = zoo;
        this.enclos = enclo;
    }
    public  Animal(String nom, float poid, Zoo zoo){
        this.nom = nom;
        this.poid = poid;
        this.zoo = zoo;
        this.enclos = null;
    }


    public String getNom(){
        return this.nom;
    }

    public float getPoid(){
        return this.poid;
    }
    public Enclos getEnclos() {
        return this.enclos;
    }
    
    public void setEnclos(Enclos newEnclos){
        this.enclos = newEnclos;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
    public void setPoid(float poid) {
        this.poid = poid;
    }

    public void setZoo(Zoo zoo) {
        this.zoo = zoo;
    }
    
    public abstract String emmitionCrie();
    
    @Override
    public String toString(){
        return this.nom + " pèse " + this.poid + "kg";
    }
}