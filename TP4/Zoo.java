import java.util.ArrayList;
import java.util.List;

class Zoo{
    private String nom ;
    private List<Animal> listeAnimaux;

    public Zoo(String nom){
        this.nom = nom;
        this.listeAnimaux = new ArrayList<>();
    }

    public String getNom() {
        return this.nom;
    }
    public void ajouterAnimal(Animal animal){
        this.listeAnimaux.add(animal);
    }
    @Override
    public String toString(){
        return "Le zoo " + this.nom + " contient: " + this.listeAnimaux;
    }
}