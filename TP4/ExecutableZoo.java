class ExecutableZoo {
    public static void main(String[] args) {
        Zoo beauval = new Zoo("Beauval");
        System.out.println(beauval.toString());
        Lion simba = new Lion("Simba", 55, beauval, false);
        System.out.println(simba.getNom());
        System.out.println(simba.getPoid());
        Enclos enclos1 = new Enclos("neclo1", 30);
        enclos1.ajouterAnimal(simba);
        simba.setEnclos(enclos1);
        beauval.ajouterAnimal(simba);
        Lion moufasa = new Lion("Mufasa", 120, beauval, true);
        Serpent kaa = new Serpent("Kaa", 30, false, beauval);
        beauval.ajouterAnimal(moufasa);
        beauval.ajouterAnimal(kaa);
        System.out.println(beauval.toString());
        System.out.println(moufasa.emmitionCrie());
    }
}