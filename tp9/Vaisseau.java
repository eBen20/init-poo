public class Vaisseau
{
    private int position;
    private int nbProjectiles;
    private int nbPointsDeVie;
    public Vaisseau(int position, int nbProjectiles, int nbPointsDeVie){
        this.position = position;
        this.nbProjectiles = nbProjectiles;
        this.nbPointsDeVie = nbPointsDeVie;
    }
    public void deplacer (int deplacement) throws DeplacementImpossible
    {
        if (this.position + deplacement < 0)
            throw new DeplacementImpossible();

}
}