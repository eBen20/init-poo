package src;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.print.DocFlavor.STRING;
import javax.swing.text.html.HTMLDocument.RunElement;

public class Rayon implements Comptable{
    private String nom;
    private List<Article> articles;
    
    public Rayon(String nom){
        this.nom = nom;
        this.articles = new ArrayList<>();
    }

    public Rayon(String nom, List<Article> articles){
        this.nom = nom;
        this.articles = articles;
    }

    public List<Article> getArticles() {
        return articles;
    }

    public String getNom() {
        return nom;
    }

    public void setArticles(List<Article> articles) {
        this.articles = articles;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void ajouteArticle(Article article) {
        this.articles.add(article);
    }

    public void tierParPrix() {
        Collections.sort(articles);
    }
    
    public Article quelPrix(String nom) throws PasDedansExeption{
        for( Article article : articles){
            if(article.getNom().equals(nom)){
                return article;
            }
        }
        throw new PasDedansExeption();
    }

    @Override
    public String toString() {
        return "Le rayon " + this.nom + " comporte " + this.articles;
    }

    @Override
    public int hashCode() {
        return this.articles.size() * 10;
    }

    @Override
    public boolean equals(Object obj) {
        if( obj == null){ return false;}
        if( obj == this){ return true;}
        if(!(obj instanceof Rayon)){ return false;}
        Rayon rayon = (Rayon) obj;
        if( this.articles.equals(rayon.articles) && this.nom.equals(rayon.nom)){
            return true;
        }
        return false;
    }

    @Override
    public int combien() {
        return this.articles.size();
    }

}
