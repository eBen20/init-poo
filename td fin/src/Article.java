package src;

public class Article implements Comparable{
    private String nom;
    private double prix;
    private double poid;

    public Article(String nom, double poid, double prix) {
        this.nom = nom;
        this.poid = poid;
        this.prix = prix;
    }
    public String getNom() {
        return nom;
    }

    public double getPoid() {
        return poid;
    }

    public double getPrix() {
        return prix;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPoid(double poid) {
        this.poid = poid;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    @Override
    public String toString() {
        return this.nom + " pèse: " + this.poid + " kilos et coûte " + this.prix + " €";
    }

    @Override
    public boolean equals(Object obj) {
        if( obj == null){ return false;}
        else if( obj == this){ return true;}
        else if(!(obj instanceof Article)){ return false;}
        Article article = (Article) obj;
        if(article.nom.equals(this.nom) && article.poid == this.poid && article.prix == this.prix){
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        int intPoid = (int) this.poid;
        int intPrix = (int) this.prix;
        return  intPoid * intPrix;
    }
    @Override
    public int compareTo(Object o) {
        Article art = (Article) o;
        if( this.prix == art.prix){return 0;}
        else if( this.prix > art.prix){ return 1;}
        else{ return -1;}
    }
    
}