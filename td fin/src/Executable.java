package src;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class Executable {
    public static void main(String[] args) {
    Article art1 = new Article("Jambon", 10, 2);
    Article art2 = new Article("cacahuette", 1, 99);
    Article art3 = new Article("Pastagua", 1.5, 30);
    System.out.println(art1.toString());
    System.out.println(art1.equals(art2));

    List<Article> lArt = new ArrayList<>(Arrays.asList(art3, art1, art2));
    // Collections.sort(lArt);
    // System.out.println(lArt);

    Rayon r = new Rayon("apero", lArt);
    try{
        System.out.println(r.quelPrix("Jambon"));
    }
    catch(PasDedansExeption e){
        System.out.println(e);
    }

    try{
        System.out.println(r.quelPrix("cocmbre"));
    }
    catch(PasDedansExeption e){
        System.out.println(e);
    }
}
}
