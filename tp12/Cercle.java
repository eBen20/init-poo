import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

public class Cercle extends Circle{
    static int RAYON_MINI = 30;
    
    public Cercle(double largeur, double hauteur){
        Circle cEnCour = new Circle(Math.random()*largeur, Math.random()*hauteur, RAYON_MINI);
        cEnCour.setFill(new Color(Math.random(), Math.random(), Math.random(), 1.0));
    }
}