public class Atouts implements Carte {
    private int valeur;

    public Atouts(int valeur){
        this.valeur = valeur;
    }
    public int getValeurInt() {
        return valeur;
    }

    @Override
    public String getType() {
        return "Atout";
    }

    @Override
    public String getValeur() {
        return Integer.toString(valeur);
    }
    
}
