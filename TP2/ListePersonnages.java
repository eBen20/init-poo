import java.util.ArrayList;
import java.util.List;

public class ListePersonnages {
    //atribut
   private List<Personnage> personnages;
    
   //méthodes
   public ListePersonnages() {
       personnages = new ArrayList<>();
   }

   public void ajoute(Personnage personne) {
       personnages.add(personne);
   }

   @Override
   public String toString() {
       return "Mes personnages : " + this.personnages;
   }

   /**
    renvoie le nom du personnage qui a le plus grand nombre de points de vie
   */
   public String maxPointsVie() {
       int max = 0;
       String nomMax = "";
       for (var personne : personnages ){
           int pointPers = personne.getPoints(); 
           if  (nomMax == "" || pointPers > max ){
               max = pointPers;
               nomMax = personne.getNom();
           }
       }
       return nomMax + " à le plus de points de vie";
   }
}