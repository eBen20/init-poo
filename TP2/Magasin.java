public class Magasin {
    private String nom;
    private boolean ouvertLundi, ouvertDimanche ;
 
    public Magasin(String nom, boolean lundi, boolean dimanche) {

        this.nom = nom;
        this.ouvertLundi = lundi;
        this.ouvertDimanche = dimanche;
     }

    public String getNom(){

        return this.nom;
    }

    public boolean getOuvertLundi(){

        return this.ouvertLundi;
    }

    public boolean getOuvertDimanche(){

        return this.ouvertDimanche;
    }
    @Override
    public String toString() {
        String ouvert = "";
        String ferme = "";
        if (getOuvertLundi()){
            ouvert = " est ouvert le lundi";
        }
        else{
            ouvert = " n'est pas ouvert le lundi";
        }

        if (getOuvertDimanche()){
            ferme = " est ouvert le Dimanche";
        }
        else{
            ferme = " est fermé le Dimanche";
        }
        return  "( Le magasin " + this.getNom() +  ouvert + " et" + ferme + ")";
    }
 }