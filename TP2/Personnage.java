public class Personnage {
    //attributs
    private String nom;
    private int pointsVie;

    //méthodes
    public Personnage (String nom) {
        this.nom = nom;
        this.pointsVie = 5; // 5 points de vie par défaut
    }

    public Personnage (String nom, int points) {
        this.nom = nom;
        this.pointsVie = points; 
    }

    public String getNom() {
        return this.nom;
    }

    public int getPoints() {
        return this.pointsVie;
    }

    @Override
    public String toString() {
        if (this.pointsVie <= 1){ 
            return this.nom + " à " + this.pointsVie + " points de vie";
        }
        return this.nom + " à " + this.pointsVie + " points de vies";
    }
 }