import java.util.ArrayList;

class ExecutableArrayList{
    public static void main(String [] args){

        ArrayList<Integer> listenombre = new ArrayList<>();
        listenombre.add(5);
        System.out.println(listenombre.contains(5)); /*renvoir true si cotient la valeur 5 dans la liste */

        ArrayList<Integer> liste2 = new ArrayList<>();
        liste2.add(3);
        liste2.add(4);
        liste2.add(5);

        listenombre.addAll(liste2); /*ajoute la liste 2 à la listenombre*/
        System.out.println(listenombre.contains(3));

        
        System.out.println(listenombre.subList(1, 4));

    } 

}