public class OuvertLundiDimanche {
    private boolean lundi;
    private boolean dimanche;

    public OuvertLundiDimanche(boolean lundi, boolean dimanche){
        this.lundi = lundi;
        this.dimanche = dimanche;
    }
    
    public boolean getOuvertLundi(){
        return this.lundi;
    }

    public boolean getOuvertDimanche(){
        return this.dimanche;
    }
    @Override
    public String toString() {
        String lundi = "";
        String dimanche = "";
        if (this.lundi){
            lundi = "est ouvert le lundi";
        }
        else{
            lundi = "est fermé le lundi";
        }
        if (this.dimanche){
            dimanche = "est ouvert le dimanche";
        }
        else{
            dimanche = "est fermé le dimanche";
        }
        return "Ce magasin " + lundi + "et " + dimanche;
    }
 }