import java.util.Map;
import java.util.HashMap;

public class DicoPersonnages {
 private Map<String, Integer> personnages;

   public DicoPersonnages() {
       this.personnages = new HashMap<String, Integer>();
   }
   public void ajoute(String nom, int points) {
       this.personnages.put(nom, points);
   }

   @Override
   public String toString() {
       return "Mes personnages : "+this.personnages;
   }
   public String maxPointsVie() {
       int maxPoints = 0;
       String nomMax = " ";
       for (String perso : this.personnages.keySet()){
           if (nomMax == " " || this.personnages.get(perso) > maxPoints){
               maxPoints = this.personnages.get(perso);
               nomMax = perso;
           }
       }
       return nomMax + " à le plus de points de vies";
   }

}