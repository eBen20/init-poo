# TP2

##  1    
  **1)** <ArrayList> possède 4 méthodes <add()>   

	
  **2)** La première méthode <add> permet d'ajouter un élément à la fin de la liste.  

  **3)** On peut tester avec  la methode <contains> avec en paramètre l'élément souhaité.
< import java.util.ArrayList;

class ExecutableArrayList{
    public static void main(String [] args){

        ArrayList<Integer> listenombre = new ArrayList<>();
        listenombre.add(5);
        System.out.println(listenombre.contains(5));
    }

} >  

  **4)** <addall> permet de rajouter une liste dans une autre liste en lui mettant en **paramèttre** une autre liste  
  Pour le tester on va tester <constains>
  < import java.util.ArrayList;

class ExecutableArrayList{
    public static void main(String [] args){

        ArrayList<Integer> listenombre = new ArrayList<>();
        listenombre.add(5);
        System.out.println(listenombre.contains(5)); /*renvoir true si cotient la valeur 5 dans la liste */

        ArrayList<Integer> liste2 = new ArrayList<>();
        liste2.add(3);
        liste2.add(4);

        listenombre.addAll(liste2); /*ajoute la liste 2 à la listenombre*/
        System.out.println(listenombre.contains(3));
    } 

} >  

  **5)** <Sublist> permet d'afficher une partie de la liste de l'indice en paramètre1 à l'indice - 1 en paramètre  

  **6)** Cela renvoie une erreur

## 3
  **1)** <put> permet de mettre une clé et une valeur dans le dictionnaire et si la clé a déjà une valeur, celle ci se fait remplacer par la nouvelle.

  **2)** On peut modifier un élément grâce à <put> comme dit à la question précédente.  

  **3)** <clear> permet de vider intégralement le dictionnaire  

  **4)** <remove> permet de supprimer une clé et sa valeur en precisant la clé en paramètre de la méthode

  