import java.util.HashMap ;
import java.util.Map ;
import java.util.ArrayList;
import java.util.List;

public class DicoMagasins{
   private Map<String, OuvertLundiDimanche> magasins;

   public DicoMagasins() {
       this.magasins = new HashMap<>();
   }

   public void ajoute(String nom, boolean lundi, boolean dimanche) {
       this.magasins.put(nom, new OuvertLundiDimanche(lundi, dimanche));
   }

   public List<String> ouvertsLeLundi() {
       List<String> ouvertLundi = new ArrayList<>();
       for (String nomMag : this.magasins.keySet() ){
           if (magasins.get(nomMag).getOuvertLundi() ){
               ouvertLundi.add(nomMag);
           }
       }
       return ouvertLundi;
   }
   @Override
   public String toString(){
       String res = "{";
       for (String keyMag : this.magasins.keySet()){
           res += keyMag + " : " + magasins.get(keyMag) + " ; " ;
        }
        res += "}";
        return res;
    }
}
